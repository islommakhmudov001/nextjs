import React from 'react';

function Feedup() {
  return (
    <div className="text-center">
      <h1 className="text-3xl font-bold">Hello FeedUp</h1>
    </div>
  );
}

export default Feedup;