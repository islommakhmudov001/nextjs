import React from 'react';

function Kfc() {
  return (
    <div className="text-center">
      <h1 className="text-3xl font-bold">Hello KFC</h1>
    </div>
  );
}

export default Kfc;