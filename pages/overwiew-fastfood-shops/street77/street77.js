import React from 'react';

function Street77() {
  return (
    <div className="text-center">
      <h1 className="text-3xl font-bold">Street 77</h1>
    </div>
  );
}

export default Street77;