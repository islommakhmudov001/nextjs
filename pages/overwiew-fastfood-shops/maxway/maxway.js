import React, {useState} from 'react';
//img
//Club Sendwich
import club1 from "../../assets/maxway.img/club1.jpg"
import club2 from "../../assets/maxway.img/clubb.png"
//lavash
import lavash from "../../assets/maxway.img/LAVASH.jpg"
import lavash2 from "../../assets/maxway.img/LAVA.jpg"
import lavash3 from "../../assets/maxway.img/04598582-e93d-43d6-a0fb-52ca3dc2b669.jpg"
import lavash4 from "../../assets/maxway.img/lavashkok.jpg"
//shaurma
import goshtlishaurma from "../../assets/maxway.img/goshtlishaurma.jpg"
import tovuqlishourma from "../../assets/maxway.img/tovuqlishourma.jpg"
//burgers
import doubleburger from "../../assets/maxway.img/burger.jpg"
import gamburger from "../../assets/maxway.img/burger2.jpg"
import chizburger from "../../assets/maxway.img/chizburger.jpg"
import doublechizburger from "../../assets/maxway.img/doublechizburger.jpg"
//maxbox
import maxshourma from "../../assets/maxway.img/maxshourma.jpg"
import maxburger from "../../assets/maxway.img/maxburger.jpg"
import maxlavash from "../../assets/maxway.img/maxboxlavash.jpg"
import maxsendwich from "../../assets/maxway.img/maxboxclub.jpg"
import xaggigoshtlik from "../../assets/maxway.img/xaggi.jpg"
import xaggikuriniy from "../../assets/maxway.img/xaggikurinniy.jpg"
import Image from "next/image";

function Maxway() {
//menu:id
const [quantity, setQuantity] = useState({
  //maxboxlar
  maxboxsendwich:1,
  lavashpopular:1,
  maxboxretro:1,
  maxboxtrend:1,
  //club sendwichlar
  sendwich:1,
  sendwichclassic:1,
  //lavashlar
  lavashclassic:1,
  lavashkuriniy:1,
  lavashachchiq:1,
  lavashsir:1,
  //shaurma
  shaurmabig:1,
  shaurmastandart:1,
  shaurmatovuq:1,
  shaurmatovuqstandart:1,
  //Gamburger
  gamburger:1,
  chizburger:1,
  doubleburger:1,
  doublechizburger:1,
  //xaggi
  xaggigoshtlik:1,
  xaggikuriniy:1

})

const handleQuantity = (item, action) => {
  setQuantity((prevState) => {
    return{
      ...prevState,

      [item]: action === 'up'
      ? quantity[item] + 1
      : quantity[item] - 1

    }
  })
}

  return (

    <div className="w-full md:h-screen h-[92vh] bg-black grid place-items-center">
      <div className=" w-9/12 xl:w-5/12 h-screen bg-slate-900 flex flex-col items-center overflow-auto">
        <h1 className="text-3xl lg:text-6xl font-bold font-serif text-white mt-10 lg:mb-16">Max Way</h1>
        {/*Maxbox*/}
        <div className="w-[620px] h-[800px] flex justify-evenly items-center flex-wrap xl:flex-nowrap lg:mt-5 mt-10">
          <div className="xl:w-64 xl:h-64 w-96 h-96 bg-white flex flex-col items-center">
            <Image className="object-cover" width={250} height={150} src={maxsendwich} alt=""/>
            <h1 className="text-black font-bold lg:text-xl text-4xl xl:mt-0 mt-5 ">Max Box Traditsia</h1>
            <p className="text-black font-bold lg:text-lg text-3xl lg:mt-0 mt-3">30.000</p>
            <div className="w-full flex items-center justify-evenly lg:mt-1 mt-5">
             <div className="pizza lg:w-24 lg:h-8 w-40 h-14 border-black border-[1px] rounded-full flex justify-evenly items-center text-black ">
               <button 
               disabled={quantity.maxboxsendwich <= 0}
               onClick={ () => handleQuantity('maxboxsendwich', 'down')}>-</button>
               <p className="font-bold">{quantity.maxboxsendwich}</p>
               <button onClick={ () => handleQuantity('maxboxsendwich', 'up')}>+</button>
             </div>
              <button className="lg:w-24 lg:h-8 w-40 h-14 bg-red-700 rounded-full text-center">
                <h1 className="text-white">Karzinka</h1>
              </button>
            </div>
          </div>

          <div className="xl:w-64 xl:h-64 w-96 h-96 bg-white flex flex-col items-center lg:mt-0 mt-8">
            <Image className="object-cover" width={250} height={150} src={maxlavash} alt=""/>
            <h1 className="text-black font-bold lg:text-xl text-4xl xl:mt-0 mt-5 ">Max Box Popular</h1>
            <p className="text-black font-bold lg:text-lg text-3xl lg:mt-0 mt-3">35.000</p>
            <div className="w-full flex items-center justify-evenly lg:mt-1 mt-5">
              <div className="lg:w-24 lg:h-8 w-40 h-14 border-black border-[1px] rounded-full flex justify-evenly items-center text-black ">
                <button disabled={quantity.lavashpopular <= 0 }
                onClick={ () => handleQuantity('lavashpopular', 'down')}
                >-</button>
                <p className="font-bold">{quantity.lavashpopular}</p>
                <button
                 onClick={ () => handleQuantity('lavashpopular', 'up')}>+</button>
              </div>
              <button className="lg:w-24 lg:h-8 w-40 h-14 bg-red-700 rounded-full text-center">
                <h1 className="text-white">Karzinka</h1>
              </button>
            </div>
          </div>
        </div>
        {/*Maxbox*/}
        <div className="w-[620px] h-[800px] flex justify-evenly items-center flex-wrap xl:flex-nowrap lg:mt-5 mt-10">
          <div className="xl:w-64 xl:h-64 w-96 h-96 bg-white flex flex-col items-center">
            <Image className="object-cover" width={250} height={150} src={maxshourma} alt=""/>
            <h1 className="text-black font-bold lg:text-xl text-4xl xl:mt-0 mt-5 ">Max Box Retro</h1>
            <p className="text-black font-bold lg:text-lg text-3xl lg:mt-0 mt-3">30.000</p>
            <div className="w-full flex items-center justify-evenly lg:mt-1 mt-5">
              <div className="lg:w-24 lg:h-8 w-40 h-14 border-black border-[1px] rounded-full flex justify-evenly items-center text-black ">
                <button disabled={quantity.maxboxretro <= 0} onClick={ () => handleQuantity('maxboxretro', 'down')}>-</button>
                <p className="font-bold">{quantity.maxboxretro}</p>
                <button onClick={ () => handleQuantity('maxboxretro', 'up')}>+</button>
              </div>
              <button className="lg:w-24 lg:h-8 w-40 h-14 bg-red-700 rounded-full text-center">
                <h1 className="text-white">Karzinka</h1>
              </button>
            </div>
          </div>

          <div className="xl:w-64 xl:h-64 w-96 h-96 bg-white flex flex-col items-center lg:mt-0 mt-8">
            <Image className="object-cover" width={250} height={150} src={maxburger} alt=""/>
            <h1 className="text-black font-bold lg:text-xl text-2xl xl:mt-0 mt-5 ">Max Box Trend</h1>
            <p className="text-black font-bold lg:text-lg text-2xl lg:mt-0 mt-3">30.000</p>
            <div className="w-full flex items-center justify-evenly lg:mt-1 mt-5">
              <div className="lg:w-24 lg:h-8 w-40 h-14 border-black border-[1px] rounded-full flex justify-evenly items-center text-black ">
                <button disabled={quantity.maxboxtrend <= 0} onClick={ () => handleQuantity('maxboxtrend', 'down')}>-</button>
                <p className="font-bold">{quantity.maxboxtrend}</p>
                <button onClick={ () => handleQuantity('maxboxtrend', 'up')}>+</button>
              </div>
              <button className="lg:w-24 lg:h-8 w-40 h-14 bg-red-700 rounded-full text-center">
                <h1 className="text-white">Karzinka</h1>
              </button>
            </div>
          </div>
        </div>
        {/*sendwich*/}
        <div className="w-[620px] h-[800px] flex justify-evenly items-center flex-wrap xl:flex-nowrap lg:mt-5 mt-10">
          <div className="xl:w-64 xl:h-64 w-96 h-96 bg-white flex flex-col items-center">
            <Image className="object-cover" width={250} height={150} src={club1} alt=""/>
            <h1 className="text-black font-bold lg:text-xl text-2xl xl:mt-0 mt-5 ">Club Sendwich</h1>
            <p className="text-black font-bold lg:text-lg text-2xl lg:mt-0 mt-3">26.000</p>
            <div className="w-full flex items-center justify-evenly lg:mt-1 mt-5">
              <div className="lg:w-24 lg:h-8 w-40 h-14 border-black border-[1px] rounded-full flex justify-evenly items-center text-black ">
                <button disabled={quantity.sendwich <= 0} onClick={ () => handleQuantity('sendwich', 'down')}>-</button>
                <p className="font-bold">{quantity.sendwich}</p>
                <button onClick={ () => handleQuantity('sendwich', 'up')}>+</button>
              </div>
              <button className="lg:w-24 lg:h-8 w-40 h-14 bg-red-700 rounded-full text-center">
                <h1 className="text-white">Karzinka</h1>
              </button>
            </div>
          </div>

          <div className="xl:w-64 xl:h-64 w-96 h-96 bg-white flex flex-col items-center lg:mt-0 mt-8">
            <Image className="object-cover" width={250} height={150} src={club2} alt=""/>
            <h1 className="text-black font-bold lg:text-xl text-2xl xl:mt-0 mt-5 ">Club Sendwich Classic</h1>
            <p className="text-black font-bold lg:text-lg text-2xl lg:mt-0 mt-3">20.000</p>
            <div className="w-full flex items-center justify-evenly lg:mt-1 mt-5">
              <div className="lg:w-24 lg:h-8 w-40 h-14 border-black border-[1px] rounded-full flex justify-evenly items-center text-black ">
                <button disabled={quantity.sendwichclassic <= 0} onClick={ () => handleQuantity('sendwichclassic', 'down')}>-</button>
                <p className="font-bold">{quantity.sendwichclassic}</p>
                <button onClick={ () => handleQuantity('sendwichclassic', 'up')}>+</button>
              </div>
              <button className="lg:w-24 lg:h-8 w-40 h-14 bg-red-700 rounded-full text-center">
                <h1 className="text-white">Karzinka</h1>
              </button>
            </div>
          </div>
        </div>
        {/*lavash*/}
        <div className="w-[620px] h-[800px] flex justify-evenly items-center flex-wrap xl:flex-nowrap lg:mt-5 mt-10">
          <div className="xl:w-64 xl:h-64 w-96 h-96 bg-white flex flex-col items-center">
            <Image className="object-cover" width={250} height={150} src={lavash4} alt=""/>
            <h1 className="text-black font-bold lg:text-xl text-2xl xl:mt-0 mt-5 ">Lavash Standart Classic</h1>
            <p className="text-black font-bold lg:text-lg text-2xl lg:mt-0 mt-3">24.000</p>
            <div className="w-full flex items-center justify-evenly lg:mt-1 mt-5">
              <div className="lg:w-24 lg:h-8 w-40 h-14 border-black border-[1px] rounded-full flex justify-evenly items-center text-black ">
                <button disabled={quantity.lavashclassic <= 0} onClick={ () => handleQuantity('lavashclassic', 'down')}>-</button>
                <p className="font-bold">{quantity.lavashclassic}</p>
                <button onClick={ () => handleQuantity('lavashclassic', 'up')}>+</button>
              </div>
              <button className="lg:w-24 lg:h-8 w-40 h-14 bg-red-700 rounded-full text-center">
                <h1 className="text-white">Karzinka</h1>
              </button>
            </div>
          </div>

          <div className="xl:w-64 xl:h-64 w-96 h-96 bg-white flex flex-col items-center lg:mt-0 mt-8">
            <Image className="object-cover" width={250} height={150} src={lavash3} alt=""/>
            <h1 className="text-black font-bold lg:text-xl text-2xl xl:mt-0 mt-5 ">Lavash Standart Tovuqlik</h1>
            <p className="text-black font-bold lg:text-lg text-2xl lg:mt-0 mt-3">22.000</p>
            <div className="w-full flex items-center justify-evenly lg:mt-1 mt-5">
              <div className="lg:w-24 lg:h-8 w-40 h-14 border-black border-[1px] rounded-full flex justify-evenly items-center text-black ">
                <button disabled={quantity.lavashkuriniy <= 0} onClick={ () => handleQuantity('lavashkuriniy', 'down')}>-</button>
                <p className="font-bold">{quantity.lavashkuriniy}</p>
                <button onClick={ () => handleQuantity('lavashkuriniy', 'up')}>+</button>
              </div>
              <button className="lg:w-24 lg:h-8 w-40 h-14 bg-red-700 rounded-full text-center">
                <h1 className="text-white">Karzinka</h1>
              </button>
            </div>
          </div>
        </div>
        {/*lavash*/}
        <div className="w-[620px] h-[800px] flex justify-evenly items-center flex-wrap xl:flex-nowrap lg:mt-5 mt-10">
          <div className="xl:w-64 xl:h-64 w-96 h-96 bg-white flex flex-col items-center">
            <Image className="object-cover" width={250} height={150} src={lavash} alt=""/>
            <h1 className="text-black font-bold lg:text-xl text-2xl xl:mt-0 mt-5 ">Lavash Standart Achchiq</h1>
            <p className="text-black font-bold lg:text-lg text-2xl lg:mt-0 mt-3">24.000</p>
            <div className="w-full flex items-center justify-evenly lg:mt-1 mt-5">
              <div className="lg:w-24 lg:h-8 w-40 h-14 border-black border-[1px] rounded-full flex justify-evenly items-center text-black ">
                <button disabled={quantity.lavashachchiq <= 0} onClick={ () => handleQuantity('lavashachchiq', 'down')}>-</button>
                <p className="font-bold">{quantity.lavashachchiq}</p>
                <button onClick={ () => handleQuantity('lavashachchiq', 'up')}>+</button>
              </div>
              <button className="lg:w-24 lg:h-8 w-40 h-14 bg-red-700 rounded-full text-center">
                <h1 className="text-white">Karzinka</h1>
              </button>
            </div>
          </div>

          <div className="xl:w-64 xl:h-64 w-96 h-96 bg-white flex flex-col items-center lg:mt-0 mt-8">
            <Image className="object-cover" width={250} height={150} src={lavash2} alt=""/>
            <h1 className="text-black font-bold lg:text-xl text-2xl xl:mt-0 mt-5 ">Lavash Standart Sirlik</h1>
            <p className="text-black font-bold lg:text-lg text-2xl lg:mt-0 mt-3">25.000</p>
            <div className="w-full flex items-center justify-evenly lg:mt-1 mt-5">
              <div className="lg:w-24 lg:h-8 w-40 h-14 border-black border-[1px] rounded-full flex justify-evenly items-center text-black ">
                <button disabled={quantity.lavashsir <= 0} onClick={ () => handleQuantity('lavashsir', 'down')}>-</button>
                <p className="font-bold">{quantity.lavashsir}</p>
                <button onClick={ () => handleQuantity('lavashsir', 'up')}>+</button>
              </div>
              <button className="lg:w-24 lg:h-8 w-40 h-14 bg-red-700 rounded-full text-center">
                <h1 className="text-white">Karzinka</h1>
              </button>
            </div>
          </div>
        </div>
        {/*shaurmatovuq*/}
        <div className="w-[620px] h-[800px] flex justify-evenly items-center flex-wrap xl:flex-nowrap lg:mt-5 mt-10">
          <div className="xl:w-64 xl:h-64 w-96 h-96 bg-white flex flex-col items-center">
            <Image className="object-cover" width={250} height={150} src={goshtlishaurma} alt=""/>
            <h1 className="text-black font-bold lg:text-xl text-2xl xl:mt-0 mt-5 ">Shaurma Big</h1>
            <p className="text-black font-bold lg:text-lg text-2xl lg:mt-0 mt-3">24.000</p>
            <div className="w-full flex items-center justify-evenly lg:mt-1 mt-5">
              <div className="lg:w-24 lg:h-8 w-40 h-14 border-black border-[1px] rounded-full flex justify-evenly items-center text-black ">
                <button disabled={quantity.shaurmabig <= 0} onClick={ () => handleQuantity('shaurmabig', 'down')}>-</button>
                <p className="font-bold">{quantity.shaurmabig}</p>
                <button onClick={ () => handleQuantity('shaurmabig', 'up')}>+</button>
              </div>
              <button className="lg:w-24 lg:h-8 w-40 h-14 bg-red-700 rounded-full text-center">
                <h1 className="text-white">Karzinka</h1>
              </button>
            </div>
          </div>

          <div className="xl:w-64 xl:h-64 w-96 h-96 bg-white flex flex-col items-center lg:mt-0 mt-8">
            <Image className="object-cover" width={250} height={150} src={goshtlishaurma} alt=""/>
            <h1 className="text-black font-bold lg:text-xl text-2xl xl:mt-0 mt-5 ">Shaurma Standart</h1>
            <p className="text-black font-bold lg:text-lg text-2xl lg:mt-0 mt-3">20.000</p>
            <div className="w-full flex items-center justify-evenly lg:mt-1 mt-5">
              <div className="lg:w-24 lg:h-8 w-40 h-14 border-black border-[1px] rounded-full flex justify-evenly items-center text-black ">
                <button disabled={quantity.shaurmastandart <= 0} onClick={ () => handleQuantity('shaurmastandart', 'down')}>-</button>
                <p className="font-bold">{quantity.shaurmastandart}</p>
                <button onClick={ () => handleQuantity('shaurmastandart', 'up')}>+</button>
              </div>
              <button className="lg:w-24 lg:h-8 w-40 h-14 bg-red-700 rounded-full text-center">
                <h1 className="text-white">Karzinka</h1>
              </button>
            </div>
          </div>
        </div>
        {/*shaurmagosht*/}
        <div className="w-[620px] h-[800px] flex justify-evenly items-center flex-wrap xl:flex-nowrap lg:mt-5 mt-10">
          <div className="xl:w-64 xl:h-64 w-96 h-96 bg-white flex flex-col items-center">
            <Image className="object-cover" width={250} height={150} src={tovuqlishourma} alt=""/>
            <h1 className="text-black font-bold lg:text-xl text-2xl xl:mt-0 mt-5 ">Shaurma Tovuqlik Big</h1>
            <p className="text-black font-bold lg:text-lg text-2xl lg:mt-0 mt-3">22.000</p>
            <div className="w-full flex items-center justify-evenly lg:mt-1 mt-5">
              <div className="lg:w-24 lg:h-8 w-40 h-14 border-black border-[1px] rounded-full flex justify-evenly items-center text-black ">
                <button disabled={quantity.shaurmatovuq <= 0} onClick={ () => handleQuantity('shaurmatovuq', 'down')}>-</button>
                <p className="font-bold">{quantity.shaurmatovuq}</p>
                <button onClick={ () => handleQuantity('shaurmatovuq', 'up')}>+</button>
              </div>
              <button className="lg:w-24 lg:h-8 w-40 h-14 bg-red-700 rounded-full text-center">
                <h1 className="text-white">Karzinka</h1>
              </button>
            </div>
          </div>

          <div className="xl:w-64 xl:h-64 w-96 h-96 bg-white flex flex-col items-center lg:mt-0 mt-8">
            <Image className="object-cover" width={250} height={150} src={tovuqlishourma} alt=""/>
            <h1 className="text-black font-bold lg:text-lg text-2xl xl:mt-0 mt-5 ">Shaurma Tovuqlik Standart</h1>
            <p className="text-black font-bold lg:text-lg text-2xl lg:mt-0 mt-3">18.000</p>
            <div className="w-full flex items-center justify-evenly lg:mt-1 mt-5">
              <div className="lg:w-24 lg:h-8 w-40 h-14 border-black border-[1px] rounded-full flex justify-evenly items-center text-black ">
                <button disabled={quantity.shaurmatovuqstandart <= 0} onClick={ () => handleQuantity('shaurmatovuqstandart', 'down')}>-</button>
                <p className="font-bold">{quantity.shaurmatovuqstandart}</p>
                <button onClick={ () => handleQuantity('shaurmatovuqstandart', 'up')}>+</button>
              </div>
              <button className="lg:w-24 lg:h-8 w-40 h-14 bg-red-700 rounded-full text-center">
                <h1 className="text-white">Karzinka</h1>
              </button>
            </div>
          </div>
        </div>
        {/*Burger*/}
        <div className="w-[620px] h-[800px] flex justify-evenly items-center flex-wrap xl:flex-nowrap lg:mt-5 mt-10">
          <div className="xl:w-64 xl:h-64 w-96 h-96 bg-white flex flex-col items-center">
            <Image className="object-cover" width={250} height={150} src={gamburger} alt=""/>
            <h1 className="text-black font-bold lg:text-xl text-2xl xl:mt-0 mt-5 ">Gamburger</h1>
            <p className="text-black font-bold lg:text-lg text-2xl lg:mt-0 mt-3">20.000</p>
            <div className="w-full flex items-center justify-evenly lg:mt-1 mt-5">
              <div className="lg:w-24 lg:h-8 w-40 h-14 border-black border-[1px] rounded-full flex justify-evenly items-center text-black ">
                <button disabled={quantity.gamburger <= 0} onClick={ () => handleQuantity('gamburger', 'down')} >-</button>
                <p className="font-bold">{quantity.gamburger}</p>
                <button onClick={ () => handleQuantity('gamburger', 'up')}>+</button>
              </div>
              <button className="lg:w-24 lg:h-8 w-40 h-14 bg-red-700 rounded-full text-center">
                <h1 className="text-white">Karzinka</h1>
              </button>
            </div>
          </div>

          <div className="xl:w-64 xl:h-64 w-96 h-96 bg-white flex flex-col items-center lg:mt-0 mt-8">
            <Image className="object-cover" width={250} height={150} src={chizburger} alt=""/>
            <h1 className="text-black font-bold lg:text-xl text-2xl xl:mt-0 mt-5 ">Chizburger</h1>
            <p className="text-black font-bold lg:text-lg text-2xl lg:mt-0 mt-3">22.000</p>
            <div className="w-full flex items-center justify-evenly lg:mt-1 mt-5">
              <div className="lg:w-24 lg:h-8 w-40 h-14 border-black border-[1px] rounded-full flex justify-evenly items-center text-black ">
                <button disabled={quantity.chizburger <= 0} onClick={ () => handleQuantity('chizburger', 'down')}>-</button>
                <p className="font-bold">{quantity.chizburger}</p>
                <button onClick={ () => handleQuantity('chizburger', 'up')}>+</button>
              </div>
              <button className="lg:w-24 lg:h-8 w-40 h-14 bg-red-700 rounded-full text-center">
                <h1 className="text-white">Karzinka</h1>
              </button>
            </div>
          </div>
        </div>
        {/*doubleburger*/}
        <div className="w-[620px] h-[800px] flex justify-evenly items-center flex-wrap xl:flex-nowrap lg:mt-5 mt-10">
          <div className="xl:w-64 xl:h-64 w-96 h-96 bg-white flex flex-col items-center">
            <Image className="object-cover" width={250} height={150} src={doubleburger} alt=""/>
            <h1 className="text-black font-bold lg:text-xl text-2xl xl:mt-0 mt-5 ">Double Burger</h1>
            <p className="text-black font-bold lg:text-lg text-2xl lg:mt-0 mt-3">29.000</p>
            <div className="w-full flex items-center justify-evenly lg:mt-1 mt-5">
              <div className="lg:w-24 lg:h-8 w-40 h-14 border-black border-[1px] rounded-full flex justify-evenly items-center text-black ">
                <button disabled={quantity.doubleburger <= 0} onClick={ () => handleQuantity('doubleburger', 'down')}>-</button>
                <p className="font-bold">{quantity.doubleburger}</p>
                <button onClick={ () => handleQuantity('doubleburger', 'up')}>+</button>
              </div>
              <button className="lg:w-24 lg:h-8 w-40 h-14 bg-red-700 rounded-full text-center">
                <h1 className="text-white">Karzinka</h1>
              </button>
            </div>
          </div>

          <div className="xl:w-64 xl:h-64 w-96 h-96 bg-white flex flex-col items-center lg:mt-0 mt-8">
            <Image className="object-cover" width={200} height={150} src={doublechizburger} alt=""/>
            <h1 className="text-black font-bold lg:text-xl text-2xl xl:mt-0 mt-5 ">Double Chizburger</h1>
            <p className="text-black font-bold lg:text-lg text-2xl lg:mt-0 mt-3">33.000</p>
            <div className="w-full flex items-center justify-evenly lg:mt-1 mt-5">
              <div className="lg:w-24 lg:h-8 w-40 h-14 border-black border-[1px] rounded-full flex justify-evenly items-center text-black ">
                <button disabled={quantity.doublechizburger <= 0} onClick={ () => handleQuantity('doublechizburger', 'down')}>-</button>
                <p className="font-bold">{quantity.doublechizburger}</p>
                <button onClick={ () => handleQuantity('doublechizburger', 'up')}>+</button>
              </div>
              <button className="lg:w-24 lg:h-8 w-40 h-14 bg-red-700 rounded-full text-center">
                <h1 className="text-white">Karzinka</h1>
              </button>
            </div>
          </div>
        </div>
        {/*xaggi*/}
        <div className="w-[620px] h-[800px] flex justify-evenly items-center flex-wrap xl:flex-nowrap lg:mt-5 mt-10">
          <div className="xl:w-64 xl:h-64 w-96 h-96 bg-white flex flex-col items-center">
            <Image className="object-cover" width={250} height={150} src={xaggigoshtlik} alt=""/>
            <h1 className="text-black font-bold lg:text-xl text-2xl xl:mt-0 mt-5 ">Xaggi Go'shtlik</h1>
            <p className="text-black font-bold lg:text-lg text-2xl lg:mt-0 mt-3">27.000</p>
            <div className="w-full flex items-center justify-evenly lg:mt-1 mt-5">
              <div className="lg:w-24 lg:h-8 w-40 h-14 border-black border-[1px] rounded-full flex justify-evenly items-center text-black ">
                <button disabled={quantity.xaggigoshtlik <= 0} onClick={ () => handleQuantity('xaggigoshtlik', 'down')}>-</button>
                <p className="font-bold">{quantity.xaggigoshtlik}</p>
                <button onClick={ () => handleQuantity('xaggigoshtlik', 'up')}>+</button>
              </div>
              <button className="lg:w-24 lg:h-8 w-40 h-14 bg-red-700 rounded-full text-center">
                <h1 className="text-white">Karzinka</h1>
              </button>
            </div>
          </div>

          <div className="xl:w-64 xl:h-64 w-96 h-96 bg-white flex flex-col items-center lg:mt-0 mt-8">
            <Image className="object-cover" width={250} height={150} src={xaggikuriniy} alt=""/>
            <h1 className="text-black font-bold lg:text-xl text-2xl xl:mt-0 mt-5 ">Xaggi Tovuqlik</h1>
            <p className="text-black font-bold lg:text-lg text-2xl lg:mt-0 mt-3">24.000</p>
            <div className="w-full flex items-center justify-evenly lg:mt-1 mt-5">
              <div className="lg:w-24 lg:h-8 w-40 h-14 border-black border-[1px] rounded-full flex justify-evenly items-center text-black ">
                <button disabled={quantity.xaggikuriniy <= 0} onClick={ () => handleQuantity('xaggikuriniy', 'dowm')}>-</button>
                <p className="font-bold">{quantity.xaggikuriniy}</p>
                <button onClick={ () => handleQuantity('xaggikuriniy', 'up')}>+</button>
              </div>
              <button className="lg:w-24 lg:h-8 w-40 h-14 bg-red-700 rounded-full text-center">
                <h1 className="text-white">Karzinka</h1>
              </button>
            </div>
          </div>
        </div>

      </div>
    </div>
  );
}

export default Maxway;