import React from 'react';
import Image from "next/image";
import maxlavash from "../../assets/maxway.img/maxboxlavash.jpg";
import maxsendwich from "../../assets/maxway.img/maxboxclub.jpg";

function Look() {
  return (
    <div className="w-full md:h-screen h-[92vh] bg-black grid place-items-center">
       <div className=" w-9/12 xl:w-5/12 h-screen bg-slate-900 flex flex-col items-center overflow-auto">
         <h1 className="text-3xl md:text-4xl font-bold font-serif text-white mt-10 mb-16">LOOOK</h1>


         <div className="flex w-full h-auto justify-evenly items-center flex-wrap">
           <div
             className="xl:w-64 xl:h-64 md:w-40 md:h-40 w-36 text-center h-36 bg-white shadow-white shadow-blue-50 shadow-md rounded-lg">
             <Image className="object-cover rounded-t-lg" width={257} height={160} src={maxlavash} alt=""/>
             <h1 className="text-black font-bold lg:text-lg text-sm lg:mt-1">Max Box Lavash</h1>
             <p className="text-black font-bold lg:text-lg text-sm lg:mt-1 mb-8 md:mb-0">35.000 so'm</p>
           </div>
           <div
             className="xl:w-64 xl:h-64 md:w-40 md:h-40 w-36 text-center h-36 bg-white shadow-white shadow-blue-50 shadow-md rounded-lg">
             <Image className="object-cover rounded-t-lg" width={257} height={160} src={maxsendwich} alt=""/>
             <h1 className="text-black font-bold lg:text-lg text-sm lg:mt-1">Max Box Traditsia</h1>
             <p className="text-black font-bold lg:text-lg text-sm lg:mt-1 mb-8 md:mb-0">30.000 so'm</p>
           </div>
         </div>
       </div>
    </div>
  );
}

export default Look;