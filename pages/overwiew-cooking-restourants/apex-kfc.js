import React from 'react';
import Link from "next/link";
import Image from "next/image";
import apex from "../assets/global.img/apex.png";
import kfc from "../assets/global.img/1200px-KFC_logo.svg.png";

function ApexKfc() {
  return (
      <div className="flex w-full justify-evenly items-center flex-wrap mt-10">
        {/*onclick links*/}
        <Link href="../overwiew-fastfood-shops/apexpizza/apex">
          <div
            className="xl:w-64 xl:h-64 md:w-40 md:h-40 w-36 h-36 bg-blue-600 flex flex-col items-center rounded-2xl pt-2 outline outline-white outline-2 ">
            <div
              className="rounded-full xl:w-32 xl:h-32 md:w-20 md:h-20 w-14 h-14 mt-3 outline outline-white outline-2 outline-offset-2">
              <Image
                className="rounded-full"
                src={apex}
                alt=""
              />
            </div>
            <button
              className="xl:w-48 xl:h-10 w-24 py-1.5 xl:mt-12 md:mt-4 mt-5 xl:text-lg text-sm font-bold
              font-serif text-black active:text-white active:bg-blue-800 active:border-2 active:text-white
              border-blue-900 border-white bg-white rounded-3xl"
            >
              Select
            </button>
          </div>
        </Link>
        {/*onclick links*/}
        <Link href="../overwiew-fastfood-shops/kfc/kfc">
          <div
            className="xl:w-64 xl:h-64 md:w-40 md:h-40 w-36 h-36 bg-blue-600 flex flex-col items-center rounded-2xl pt-2 outline outline-white outline-2 ">
            <div
              className="rounded-full xl:w-32 xl:h-32 md:w-20 md:h-20 w-14 h-14 mt-3 outline outline-white outline-2 outline-offset-2">
              <Image
                className="rounded-full"
                src={kfc}
                alt=""
              />
            </div>
            <button
              className="xl:w-48 xl:h-10 w-24 py-1.5 xl:mt-12 md:mt-4 mt-5 xl:text-lg text-sm font-bold
              font-serif text-black active:text-white active:bg-blue-800 active:border-2 active:text-white
              border-blue-900 border-white bg-white rounded-3xl"
            >
              Select
            </button>
          </div>
        </Link>
      </div>
  );
}

export default ApexKfc;