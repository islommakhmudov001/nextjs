//overwiew-js-files
import MaxwayLoook from "../overwiew-cooking-restourants/maxway-look"
import ApexKfc from "../overwiew-cooking-restourants/apex-kfc"
import StreetFeedup from "../overwiew-cooking-restourants/StreetFeed"
import EvosOqtepa from "../overwiew-cooking-restourants/evos-oqtepa"
import WokTarnov from "../overwiew-cooking-restourants/wok-tarnov"
import GreekBlack from "../overwiew-cooking-restourants/greek-black"

function Cooking() {
  return (
    <div className="w-full md:h-screen h-[92vh] bg-black grid place-items-center">
      <div className=" w-9/12 xl:w-5/12 h-screen bg-slate-900 flex flex-col items-center overflow-auto">
        <h1 className="text-3xl md:text-4xl font-bold font-serif text-white mt-10 mb-16">Choose Restaurant</h1>

        < MaxwayLoook/>
        < ApexKfc/>
        < StreetFeedup/>
        <EvosOqtepa/>
        <WokTarnov/>
        <GreekBlack/>

      </div>
    </div>
  );
}

export default Cooking;