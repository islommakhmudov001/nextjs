//components
import Cards from "../overwiew-contacts/cards";
import Link from "next/link";
function Users () {
  return (
    <div
      className="w-full md:h-screen h-[92vh]
       bg-black grid place-items-center"
    >
      <div
        className=" w-9/12 xl:w-5/12 h-screen
         bg-slate-900 flex flex-col items-center
          overflow-auto">

        <h1
          className="text-2xl md:text-4xl font-bold
           font-serif text-white mt-10">
          Office Users
        </h1>

        <Cards />

        <Link href="../cooking-main/Cooking">
          <button
            className=" w-9/12 md:h-16 h-8
            bg-blue-600 md:text-3xl
           text-xl text-amber-50 font-bold
           font-bold mt-7 mb-7 ">
            Checked
          </button>
        </Link>

      </div>
    </div>);
}

export default Users;