import React, {useState} from 'react';
//img
import contact from "../assets/global.img/blank-profile-picture-973460__340.webp"
import arrow from "../assets/global.img/arrow.webp"
import Image from "next/image";
import Link from "next/link";
import colors from "tailwindcss/colors";

function Cards() {

  let users = [
    {
      id: 1,
      name: "Dilkush Vohobov",
      level: "Senior Backend",
      isActive: false,
    },
    {
      id: 2,
      name: "Abdurahmon Olimov",
      level: "Senior Ui/Ux",
      isActive: false,
    },
    {
      id: 3,
      name: "Asliddin Usmonov",
      level: "Senior FrontEnd",
      isActive: false,
    },
    {
      id: 4,
      name: "Maxfiy Dasturchi",
      level: "Project Manager",
      isActive: false,
    },
    {
      id: 5,
      name: "Sardor Abdujalolov",
      level: "Middle Ui/UX",
      isActive: false,
    },
    {
      id: 6,
      name: "Oybek Tojiyev",
      level: "Junior Front-End",
      isActive: false,
    },
    {
      id: 7,
      name: "Mustafo Sadikov",
      level: "Flutter",
      isActive: false,
    },
    {
      id: 8,
      name: "Sanjar Omonov",
      level: "UI Programmer",
      isActive: false,
    },
    {
      id: 9,
      name: "Islom Makhmudov",
      level: "Junior Front-End",
      isActive: false,
    },
    {
      id: 10,
      name: "Nozim Jurayev",
      level: "Junior Back-end",
      isActive: false,
    },
    {
      id: 11,
      name: "Adham Abdusamadov",
      level: "Junior Back-end",
      isActive: false,
    },
    {
      id: 12,
      name: "Sirojiddin Yunusov",
      level: "Junior Front-End",
      isActive: false,
    },
    {
      id: 13,
      name: "Shahzoda Rustamova",
      level: "Junior PM",
      isActive: false,
    },
  ];

  return users.map( x =>
    <div onClick={event => {event.currentTarget.classList.toggle("bg-green-500")}}
      className="w-10/12 h-screen bg-white mt-7 flex items-center justify-between">
      {/*user-icon*/}
      <div className="md:w-2/12 w-6 md:h-24 h-14 flex ml-2 md:ml-0 items-center justify-center">
        <Image
          width={60}
          height={60}
          className="rounded-full "
          src={contact}
          alt=""
        />
      </div>
      {/*user-name*/}
      <div className=" md:w-6/12 text-center">
        <ul>
          <li
            className="xl:text-2xl md:text-xl font-serif font-bold
             text-sm text-[#505050]">
            {x.name}
          </li>
        </ul>
      </div>
      {/*arrow-icon*/}
      <div className="md:w-1/12 w-5">
        <Image
          width={35}
          height={35}
          src={arrow}
          alt=""
        />
      </div>
    </div>
  )
};
export default Cards;